package day05;

import java.util.LinkedList;

public class Test {

	public static void main(String[] args) {
		LinkedList<String> queue1 = new LinkedList<>();
		queue1.add("�ks");
		queue1.add("kaks");
		queue1.add("kolm");
		queue1.add("neli");
		queue1.add("viis");
		queue1.add("kuus");
		queue1.add("seitse");
		queue1.add("kaheksa");
		// queue1.forEach(x -> { x = x.toUpperCase(); });
		queue1.replaceAll(x -> {
			return x.substring(0, 1).toUpperCase() + x.substring(1, x.length());
		});
		System.out.println(queue1);
		while (!queue1.isEmpty()) {
			// System.out.println(queue1.remove());
			System.out.println(queue1.pop());
		}
		System.out.println(queue1);


	}

}
