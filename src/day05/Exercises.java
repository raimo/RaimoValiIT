package day05;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sun.org.apache.xml.internal.serializer.utils.StringToIntTable;

public class Exercises {

	public static void main(String[] args) {
		// ALT + SHIFT + R - asendab kõik samad nimega muutujad/meetoodid
		// ÜLESANNE 1
		String hashtag = "#";

		int a = 5;

		while (a >= 0) {
			decreaseAndPrint(a, hashtag);
			System.out.println();
			a--;
		}

		// ÜLESANNE 2
		System.out.println("- - - - - - - - - - - - - - - - - - -");
		int b = 1;
		int c = 6;

		while (b <= c) {
			increaseAndPrint(b, hashtag);
			System.out.println();
			b++;
		}

		// ÜLESANNE 3
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		String txt1 = " ";
		String txt2 = "@";

		int start = 0;
		int end = 4;
		int end2 = 5;

		while (start < end2) {
			doAndPrint(end, start, txt1, txt2);
			System.out.println();
			start++;
			end--;
		}

		// ÜLESANNE 4
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		char[] charArray = args[0].toCharArray();
		int length = charArray.length - 1;

		while (length >= 0) {
			System.out.print(charArray[length]);
			length--;
		}
		System.out.println();

		// ÜLESANNE 5
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		int score = Integer.parseInt(args[0]);
		gradeTheStudent(score);

		// ÜLESANNE 6
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		Double[][] dbl = { { 4d, 3d }, { 5d, 9d }, { 4d, 8d } };
		for (Double[] x : dbl) {
			System.out.println(Math.sqrt(Math.pow(x[0], 2) + Math.pow(x[1], 2)));
		}

		// ÜLESANNE 7
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		String[][] info = { { "Estonia", "Tallinn", "Jüri Ratas" }, { "Latvia", "Riga", "Māris Kučinskis" },
				{ "Finland", "Helsinki", "Juha Sipilä" }, { "Sweden", "Stockholm", "Stefan Löfven" },
				{ "Norway", "Oslo", "Erna Solberg" }, { "Germany", "Berlin", "Angela Merkel" },
				{ "France", "Paris", "Édouard Philippe" }, { "Poland", "Warsaw", "Mateusz Morawieck" },
				{ "Italy", "Rome", "Paolo Gentiloni" }, { "Lithuania", "Vilnius", "Saulius Skvernelis" } };

		for (String[] x : info) {
			System.out.println(x[2]);
		}
		for (String[] x : info) {
			System.out.println("Country: " + x[0] + "; Capital: " + x[1] + "; Prime minister: " + x[2]);
		}

		Object[][] info2 = {
				{ "Estonia", "Tallinn", "Jüri Ratas",
						new String[] { "Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish" } },
				{ "Latvia", "Riga", "Māris Kučinskis",
						new String[] { "Latvian", "Russian", "Belarusian", "Ukrainian", "Polish" } },
				{ "Finland", "Helsinki", "Juha Sipilä",
						new String[] { "Finnish", "Russian", "Swedish", "Norwegian", "Polish" } },
				{ "Sweden", "Stockholm", "Stefan Löfven",
						new String[] { "Swedish", "Norwegian", "Finnish", "Russian" } },
				{ "Norway", "Oslo", "Erna Solberg", new String[] { "Norwegian", "Swedish", "Finnish", "Russian" } },
				{ "Germany", "Berlin", "Angela Merkel", new String[] { "German", "French", "Polish", "English" } },
				{ "France", "Paris", "Édouard Philippe",
						new String[] { "French", "German", "Polish", "English", "Spanish" } },
				{ "Poland", "Warsaw", "Mateusz Morawieck",
						new String[] { "Polish", "Russian", "Lithuanian", "English" } },
				{ "Italy", "Rome", "Paolo Gentiloni", new String[] { "Italian", "French", "Polish", "English" } },
				{ "Lithuania", "Vilnius", "Saulius Skvernelis",
						new String[] { "Lithuanian", "Russian", "Polish", "English" } } };

		for (int i = 0; i < info2.length; i++) { // käin massiivi läbi vastavalt ridade arvule (.size)
			Object[] info2Element = info2[i]; // loon uue objekti, mis on massiv
			System.out.println(info2Element[0] + ":"); // prindin selle objekti esimese elemendi välja
			String[] languages = (String[]) info2[i][3]; // muudan objekti stringiks
			for (String language : languages) {
				System.out.println("1       " + language);
			}

		}

		// ÜLESANNE 8
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		List<Object> names = new ArrayList<>();
		names.add(Arrays.asList("Estonia", "Tallinn", "Jüri Ratas",
				new String[] { "Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish" }));
		names.add(Arrays.asList("Latvia", "Riga", "Māris Kučinskis",
				new String[] { "Latvian", "Russian", "Belarusian", "Ukrainian", "Polish" }));
		names.add(Arrays.asList("Finland", "Helsinki", "Juha Sipilä",
				new String[] { "Finnish", "Russian", "Swedish", "Norwegian", "Polish" }));
		names.add(Arrays.asList("Sweden", "Stockholm", "Stefan Löfven",
				new String[] { "Swedish", "Norwegian", "Finnish", "Russian" }));
		names.add(Arrays.asList("Norway", "Oslo", "Erna Solberg",
				new String[] { "Norwegian", "Swedish", "Finnish", "Russian" }));
		names.add(Arrays.asList("Germany", "Berlin", "Angela Merkel",
				new String[] { "German", "French", "Polish", "English" }));
		names.add(Arrays.asList("France", "Paris", "Édouard Philippe",
				new String[] { "French", "German", "Polish", "English", "Spanish" }));
		names.add(Arrays.asList("Poland", "Warsaw", "Mateusz Morawieck",
				new String[] { "Polish", "Russian", "Lithuanian", "English" }));
		names.add(Arrays.asList("Italy", "Rome", "Paolo Gentiloni",
				new String[] { "Italian", "French", "Polish", "English" }));
		names.add(Arrays.asList("Lithuania", "Vilnius", "Saulius Skvernelis",
				new String[] { "Lithuanian", "Russian", "Polish", "English" }));

		for (int i = 0; i < names.size(); i++) { // käin läbi massiivi terves suuruses (10)
			List<Object> x = (List<Object>) names.get(i); // loon uue ajutise listi 'x' iga "rea" kohta (sest tegemist
															// OBJEKTi listiga)
			System.out.println(x.get(0) + ": "); // võtan ajutisest listist esimese elemendi ja prindin välja
			String[] languages2 = (String[]) x.get(3); // muudan ajutise listi neljanda elemendi objektist tekstide
														// massiiviks
			for (int j = 0; j < languages2.length; j++) {
				System.out.println("2         " + languages2[j]);
			}

		}
		// ÜLESANNE 9
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		Map<String, String[]> names9 = new HashMap<>();
		names9.put("Estonia", new String[] { "Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish" });
		names9.put("Latvia", new String[] { "Latvian", "Russian", "Belarusian", "Ukrainian", "Polish" });
		names9.put("Finland", new String[] { "Finnish", "Russian", "Swedish", "Norwegian", "Polish" });
		names9.put("Sweden", new String[] { "Swedish", "Norwegian", "Finnish", "Russian" });
		names9.put("Norway", new String[] { "Norwegian", "Swedish", "Finnish", "Russian" });
		names9.put("Germany", new String[] { "German", "French", "Polish", "English" });
		names9.put("France", new String[] { "French", "German", "Polish", "English", "Spanish" });
		names9.put("Poland", new String[] { "Polish", "Russian", "Lithuanian", "English" });
		names9.put("Italy", new String[] { "Italian", "French", "Polish", "English" });
		names9.put("Lithuania", new String[] { "Lithuanian", "Russian", "Polish", "English" });

		for (String key : names9.keySet()) { // käin võtmemassiivi läbi elemendi kaupa
			System.out.println(key + ":"); // prindin elemendid
			for (String lang : names9.get(key)) { // käin võtmetele vastavad elemendid läbi
				System.out.println("3    " + lang);
			}
			System.out.println();
		}

		// ÜLESANNE 10
		System.out.println("- - - - - - - - - - - - - - - - - - -");

		LinkedList<Object> queue = new LinkedList<>();
		queue.add(
				Arrays.asList("Estonia", new String[] { "Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish" }));
		queue.add(Arrays.asList("Latvia", new String[] { "Latvian", "Russian", "Belarusian", "Ukrainian", "Polish" }));
		queue.add(Arrays.asList("Finland", new String[] { "Finnish", "Russian", "Swedish", "Norwegian", "Polish" }));
		queue.add(Arrays.asList("Sweden", new String[] { "Swedish", "Norwegian", "Finnish", "Russian" }));
		queue.add(Arrays.asList("Norway", new String[] { "Norwegian", "Swedish", "Finnish", "Russian" }));
		queue.add(Arrays.asList("Germany", new String[] { "German", "French", "Polish", "English" }));
		queue.add(Arrays.asList("France", new String[] { "French", "German", "Polish", "English", "Spanish" }));
		queue.add(Arrays.asList("Poland", new String[] { "Polish", "Russian", "Lithuanian", "English" }));
		queue.add(Arrays.asList("Italy", new String[] { "Italian", "French", "Polish", "English" }));
		queue.add(Arrays.asList("Lithuania", new String[] { "Lithuanian", "Russian", "Polish", "English" }));

		while (!queue.isEmpty()) { // töötab kuni list on tühi
			List<Object> y = (List<Object>) queue.remove(); // loon listi esimesest positsioonist uue listi nimega y
			System.out.println(y.get(0) + ":"); // võtan y'st 0 indeksiga elemendi
			String[] languages3 = (String[]) y.get(1); // loon uue teksti massiivi, mis tuleb y'st indeksiga 1
			for (String lang : languages3) { // käin teksti massiivi läbi elemendi kaupa
				System.out.println("4    " + lang); // prindin iga elemendi välja
			}
		}
	}

	private static void decreaseAndPrint(int rep, String text) {
		while (rep >= 0) {
			System.out.print(text);
			rep--;

		}
	}

	private static void increaseAndPrint(int rep, String text) {
		int i = 0;
		while (i < rep) {
			System.out.print(text);
			i++;

		}
	}

	private static void doAndPrint(int end, int start, String text1, String text2) {
		while (end - 1 >= 0) {
			System.out.print(text1);
			end--;
		}

		while (start >= 0) {
			System.out.print(text2);
			start--;
		}

	}

	private static void gradeTheStudent(int score) {

		if (score > 90) {
			System.out.println("PASS - hinne 5, punkti summa: " + score);
		} else if (score > 80) {
			System.out.println("PASS - hinne 4, punkti summa: " + score);
		} else if (score > 70) {
			System.out.println("PASS - hinne 3, punkti summa: " + score);
		} else if (score > 60) {
			System.out.println("PASS - hinne 2, punkti summa: " + score);
		} else if (score > 50) {
			System.out.println("PASS - hinne 1, punkti summa: " + score);
		} else {
			System.out.println("FAIL, said alla 51 punkti!");
		}
	}
}
