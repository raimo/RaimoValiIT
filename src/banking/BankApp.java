package banking;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {

	public static void main(String[] args) throws IOException {

		Scanner inputScanner = new Scanner(System.in);
		System.out.println("Banking system 101 starting up....");
		if (!Account.importClients(args[0])) {
			return;
		}
		// System.out.println(Account.getClients());

		while (true) { // ts�kkel mis ei l�ppe kunagi
			System.out.println("Please enter command:");
			System.out.println("[TRANSFER FROM_ACC TO_ACC SUM]  [BALANCE ACC_NUM]  [BALANCE FIRST_NAME LAST_NAME]");

			// Kuula sisendit...
			String input = inputScanner.nextLine(); // k�sib kasutaja poolset sisendit. Rida on valmis kui vajutatakse
													// enterit (uus rida)
			// Teosta operatsioon...
			if (input.startsWith("TRANSFER")) {
				System.out.println("Starting money transfer...");
				String[] inputParts = input.split(" ");
				Account.transfer(inputParts[1], inputParts[2], Integer.parseInt(inputParts[3]));
			} else if (input.startsWith("BALANCE")) {
				System.out.println("Displaying account balance...");
				String[] inputParts = input.split(" ");
				if (inputParts.length == 2) {
					System.out.println("Searching by account number...");
					Account.displayClientInfo(inputParts[1]);
				} else {
					System.out.println("Searching by name and last name...");
					Account.displayClientInfo(inputParts[1], inputParts[2]);
				}
			} else {
				System.out.println("Incorrect command");
			}
		}

	}

}
