package day07;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Decryptor extends Cryptor {

	public Decryptor(String filePath) throws IOException {
		super(filePath);

	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		Map<String, String> dictionary = new HashMap<>();

		for (String dictLine : fileLines) {
			String[] lineParts = dictLine.split(", ");
			dictionary.put(lineParts[1], lineParts[0]);
		}
		return dictionary;
	}

	@Override
	public String translate(String text) {
		if (text != null) {
			String decryptedText = "";
			for (int i = 0; i < text.length() / 32; i++) {
				String encryptedChar = text.substring(i * 32, i * 32 + 32);
				decryptedText = decryptedText + dictionary.get(encryptedChar);
			}
			return decryptedText;
		}
		return null;
	}
}