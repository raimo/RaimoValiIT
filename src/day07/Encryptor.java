package day07;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Encryptor extends Cryptor {

	public Encryptor(String filePath) throws IOException {
		super(filePath);

	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		Map<String, String> dictionary = new HashMap<>();

		for (String dictLine : fileLines) {
			String[] lineParts = dictLine.split(", ");
			dictionary.put(lineParts[0], lineParts[1]);
		}
		return dictionary;
	}

	@Override
	public String translate(String text) {
		if (text != null) {
			String translatedText = "";
			char[] textChars = text.toCharArray();
			for (char c : textChars) {
				String letter = String.valueOf(c).toUpperCase();
				translatedText = translatedText + this.dictionary.get(letter);
			}
			return translatedText;
		}
		return null;
	}
}
