package day07;

import java.io.IOException;

public class OoCryptoApp {

	public static void main(String[] args) throws IOException {
		// V�ta sisendtekst
		// Loe sisse s�nastik(failist).
		// Teosta konverteerimine.
		System.out.println("Algne tekst:");
		System.out.println(args[0]);
		
		Cryptor encryptor = new Encryptor ("C:/Users/opilane/Desktop/Vali IT/Programmi failid/alfabeet.txt");
		String encryptedText = encryptor.translate(args[0]);
		
		System.out.println("Kr�pteeritud tekst:");
		System.out.println(encryptedText);
		
		Cryptor decryptor = new Decryptor ("C:/Users/opilane/Desktop/Vali IT/Programmi failid/alfabeet.txt");
		String decryptedText = decryptor.translate(encryptedText);
		
		System.out.println("Dekr�pteeritud tekst:");
		System.out.println(decryptedText);
	}

}
