package day07;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CryptoApp {

	public static void main(String[] args) throws IOException {
		String text = args[0];
		List<String> dictionary = readAlphabet();
		Map<String, String> encDictionary = generateEncDictionary(dictionary);
		Map<String, String> decDictionary = generateDecDictionary(dictionary);
		String encryptedText = encrypt(encDictionary, text);
		String decryptedText = decrypt(decDictionary, encryptedText);

		System.out.println("Encrypted message:");
		System.out.println(encryptedText);
		System.out.println("Decrypted message:");
		System.out.println(decryptedText);
	}

	private static List<String> readAlphabet() throws IOException {
		Path path = Paths.get("C:/Users/opilane/Desktop/Vali IT/Programmi failid/alfabeet.txt");
		return Files.readAllLines(path);
	}

	private static Map<String, String> generateEncDictionary(List<String> dictionaryLines) {
		Map<String, String> dictionary = new HashMap<>();

		for (String dictLine : dictionaryLines) {
			String[] lineParts = dictLine.split(", ");
			dictionary.put(lineParts[0], lineParts[1]);
		}
		return dictionary;
	}

	private static Map<String, String> generateDecDictionary(List<String> dictionaryLines) {
		Map<String, String> dictionary = new HashMap<>();
		for (String dictLine : dictionaryLines) {
			String[] lineParts = dictLine.split(", ");
			dictionary.put(lineParts[1], lineParts[0]);
		}
		return dictionary;

	}

	private static String encrypt(Map<String, String> dictionary, String text) {
		if (text != null) {
			String translatedText = "";
			char[] textChars = text.toCharArray();
			for (char c : textChars) {
				String letter = String.valueOf(c).toUpperCase();
				translatedText = translatedText + dictionary.get(letter);
			}
			return translatedText;
		}

		return null;

	}

	private static String decrypt(Map<String, String> dictionary, String encryptedText) {
		if (encryptedText != null) {
			String decryptedText = "";
			for (int i = 0; i < encryptedText.length() / 32; i++) {
				String encryptedChar = encryptedText.substring(i * 32, i * 32 + 32);
				decryptedText = decryptedText + dictionary.get(encryptedChar);
			}
			return decryptedText;
		}

		return null;

	}
}
