package day03;

public class Color {

	public static void main(String[] args) {

		if (args[0].equalsIgnoreCase("Green")) {
			System.out.println("Driver can drive a car.");
		} else if (args[0].equalsIgnoreCase("Yellow")) {
			System.out.println("Driver has to be ready to stop the car or to start driving.");

		} else if (args[0].equalsIgnoreCase("Red")) {
			System.out.println("Driver has to stop car and wait for green light");
		} else
			System.out.println("Not valid color!");

		String color = args[0].toUpperCase();

		switch (color) {
		case "GREEN":
			System.out.println("Driver can drive a car.");
			break;
		case "YELLOW":
			System.out.println("Driver has to be ready to stop the car or to start driving.");
			break;
		case "RED":
			System.out.println("Driver has to stop car and wait for green light");
			break;
		default:
			System.out.println("Not valid color!");
		}

	}

}
