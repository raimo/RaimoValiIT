package day03;

import java.util.Scanner;

public class Day03 {

	private final static double VAT_PERCENTAGE = 0.25d;

	public static void main(String[] args) { // meetod ei tagasta mitte midagi, sest on void

		StringBuilder sb = new StringBuilder(); // m��ran uue StringBuilder muutuja objekti, millel on StringBuilder
												// omadused
		String president1 = "Konstantin P�ts";
		String president2 = "Lennart Meri";
		String president3 = "Arnolt R��tel";
		String president4 = "Toomas Hendrik Ilves";
		String president5 = "Kersti Kaljulaid";

		// append meetod tagastab StringBuilderi, ehk iseenda, seega saab j�tkata
		// l�putult
		sb.append(president1).append(", ").append(president2).append(", ").append(president3).append(", ")
				.append(president4).append(" ja ").append(president5).append(" on Eesti presidendid.");

		System.out.println(sb.toString());

		// String[] stringArray1 = {"Konstantin P�ts", "Lennart Meri", "Arold R��tel"};
		// StringBuilder sb2= new StringBuilder();
		// for(String item : stringArray1) {
		// sb2.append(item);
		// }
		// System.out.println(sb2.toString());
		printSentence("Rehepapp");

		teeTeineYlesanne();

		System.out.println(args[0]); 
		System.out.println(args[1]);

		int in1 = Integer.parseInt(args[0]);
		float in2 = Float.parseFloat(args[1]);
		float sum = in1 + in2;
		System.out.println(sum);

		String sumStr = ((Float) sum).toString();
		String sumStr2 = String.valueOf(sum); // muudab primitiivse muutujat��bi stringiks
		System.out.println(sumStr);
		System.out.println(sumStr2);

		// String split meetodi n�ide
		String str3 = "tekst1 tekst2 tekst3 tekst4 tekst5";
		String[] str3Arr = str3.split(", ");
		for (String strItem : str3Arr) {
			System.out.println(strItem);
		}

		double price = 100.0d;
		price = price + price * VAT_PERCENTAGE;
		System.out.println(price);
	}

	private static void printSentence(String bookTitle) { // privaatne funktsioon mis on ligip��setav ainult selle
															// klassi seest
		String text1 = "Raamatu pealkiri on \"" + bookTitle + "\".";
		System.out.println(text1);

	}

	private static void teeTeineYlesanne() { // ei ole sisendparameetreid

		String s1 = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
		Scanner sc = new Scanner(s1);
		sc.useDelimiter("Rida: ");
		while (sc.hasNext()) {
			System.out.println(sc.next());
		}

	}

}
