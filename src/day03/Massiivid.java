package day03;

public class Massiivid {

	public static void main(String[] args) {
		
		// �hem��tmeline massiiv
		String [] user = {"Tanel", "Tuisk", "Elva"};
		
		// Kahem��tmeline massiiv
		String[][] users = { 
				{ "Marek", "Lints", "Tallinn" }, 
				{ "Leida", "Kuusk", "Tartu" },
				{ "Tanel", "Tuisk", "Elva" } 
			};
		// Osaliselt kolmem��tmeline massiiv (objektide massiv)
		Object[][] users2 = { 
				{ "Marek", "Lints", new String[] {"Tallinn", "Tartu"} }, 
				{ "Leida", "Kuusk", "Tartu" },
				{ "Tanel", "Tuisk", "Elva" },
				user
			};
		for (String[] u: users) { 
			for (String userData :u) {
				System.out.print(userData + " ");
			}
			System.out.println();
		}
		
		
		// �lesanne 7
		int[] m;
		m = new int[5];
		m[0] = 1;
		m[1] = 2;
		m[2] = 3;
		m[3] = 4;
		m[4] = 5;
		doExercise7(m);

		// �lesanne 8

		String[] city = { "Tallinn", "Helsinki", "Madrid", "Paris" };

		// �lesanne 9

		int[][] mas2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, 0 } };
		System.out.println(mas2[1][1]);
 
		// �lesanne 10

		String[][] city2 = { { "Tallinn", "Tartu", "Valga", "V�ru" }, 
				{ "Stockholm", "Uppsala", "Lund", "K�ping" },
				{ "Helsinki", "Espoo", "Hanko", "J�ms�" } };
		System.out.println(city2[2][3]);

		String[][] c = new String[3][4];
		c[0][0] = "Tallinn";
		c[0][1] = "Tartu";
		c[0][2] = "Valga";
		c[0][3] = "V�ru";
		c[1][0] = "Stockholm";
		c[1][1] = "Uppsala";
		c[1][2] = "Lund";
		c[1][3] = "K�ping";
		c[2][0] = "Helsinki";
		c[2][1] = "Espoo";
		c[2][2] = "Hanko";
		c[2][3] = "J�ms�";
		System.out.println(c[2][2]);
	}

	private static void doExercise7(int[] mass) {
		System.out.println(mass[0]);
		System.out.println(mass[2]);
		System.out.println(mass[mass.length-1]);
	}

}
