package day03;

public class ConditionApp {

	public static void main(String[] args) {

		int number = Integer.parseInt(args[0]);
		boolean oddityCheck = number % 2 == 0;
		if (oddityCheck) {
			System.out.println("Etteantud arv " + number + " on paaris");
		} else {
			System.out.println("Etteantud arv " + number + " on paaritu");
		}

		String paaris = (number % 2 == 0) ? "Etteantud arv " + number + " on paaris"
				: "Etteantud arv " + number + " on paaritu";
		System.out.println(paaris);

		System.out.println((number % 2 == 0) ? "Etteantud arv " + number + " on paaris"
				: "Etteantud arv " + number + " on paaritu");

		int doorStatus = 4;
		switch (doorStatus) {
		case 0:
			System.out.println("Door is closed");
			break;
		case 1:
			System.out.println("Door is little bit open.");
			break;
		case 2:
			System.out.println("Door is almost open.");
			break;
		case 3:
			System.out.println("Door is very open.");
			break;
		case 4:
			System.out.println("Door is gone!");
			break;
		default:
			System.out.println("Could not coplete the command");
			break;
		}
		
		
	}

}
