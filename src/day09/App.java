package day09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import day08.AudiS4;
import day08.Car;

public class App {

	public static void main(String[] args) {

		Car audis4_1 = new AudiS4();
		audis4_1.setColor("Nagano blue");
		audis4_1.setMaxSpeed(250);
		audis4_1.setSeatCount(5);

		Car audis4_2 = new AudiS4();
		audis4_2.setColor("Nagano blue");
		audis4_2.setMaxSpeed(250);
		audis4_2.setSeatCount(5);

		System.out.println("Audi S4_1 hashcode: " + audis4_1.hashCode());
		System.out.println("Audi S4_2 hashcode: " + audis4_2.hashCode());
		System.out.println(audis4_1 == audis4_2); // v�rdleb kahe objekti pointereid omavahel
		System.out.println(audis4_1.equals(audis4_2)); // v�rdleb kahe objekti omadusi omavahel (vastavalt equals
														// meetodile)
	
		Car car1 = new AudiS4();
		car1.setColor("Blue");
		car1.setMaxSpeed(250);
		car1.setSeatCount(5);

		Car car2 = new AudiS4();
		car2.setColor("Red");
		car2.setMaxSpeed(200);
		car2.setSeatCount(4);
		
		Car car3 = new AudiS4();
		car3.setColor("White");
		car3.setMaxSpeed(150);
		car3.setSeatCount(7);

		Car car4 = new AudiS4();
		car4.setColor("Silver");
		car4.setMaxSpeed(300);
		car4.setSeatCount(2);
	
		System.out.println(car1.compareTo(car3));
		

		
	}
	
	

}
