package day02;

//import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Day02 {

	public static void main(String[] args) {

		// int a = 1;
		// int b = 1;
		// int c = 3;
		// System.out.println(a == b);
		// System.out.println(a == c);
		// a = c;
		// System.out.println(a == b);
		// System.out.println(a == c);
		//
		// short x1 = 10;
		// short x2 = 20;
		// short y1 = ++x1;
		// short y2 = x2++;
		// System.out.println(x1);
		// System.out.println(x2);
		// System.out.println(y1); // suurendab x1 v��rtust peale ekraanile printimist,
		// ehk n�iliselt number ei
		// // muutu
		// System.out.println(y2); // suurendab x2 v��rtust enne ekraanile printimist,
		// ehk number muutub
		//
		// int d = 18 % 3;
		// int e = 19 % 3;
		// int f = 20 % 3;
		// int g = 21 % 3;
		// System.out.println(d);
		// System.out.println(e);
		// System.out.println(f);
		// System.out.println(g);
		//
		// String st1 = new String("Midagi 1");
		// String st2 = new String("MIDAGI 1");
		// System.out.println(st1.equals(st2));
		// System.out.println(st1.equalsIgnoreCase(st2));
		//
		// int au = 3;

		float fl1 = 456.78f;
		System.out.println(fl1);

		String st1 = "test";
		System.out.println(st1);

		// int i1 = 4;
		// boolean bo1 = i1 == 4;

		boolean b1 = true;
		System.out.println(b1);

		// Muutuja, mis viitab numbrite kogumile.
		// Variant 1:
		int[] numArray1 = new int[4]; // numbrite kogum, mis sisaldab 4 numbrit
		numArray1[0] = 5;
		numArray1[1] = 91;
		numArray1[2] = 304;
		numArray1[3] = 405;
		System.out.println(numArray1[0]);
		System.out.println(numArray1[1]);
		System.out.println(numArray1[2]);
		System.out.println(numArray1[3]);

		// Variant 2:
		//int[] numArray2 = { 5, 91, 304, 405 };
		for (int i = 0; i < numArray1.length; i++) {
			System.out.println(numArray1[i]);
		}

		float[] decArray1 = { 56.7f, 45.8f, 91.2f };
		for (int i = 0; i < decArray1.length; i++) {
			System.out.println(decArray1[i]);
		}

		String st2 = "a";
		char ch1 = 'a';
		System.out.println(st2);
		System.out.println(ch1);

		// Erinevat t��pi muutujate kogum
		//String[] textArray1 = { "see on eseimene v��rtus", "67", "58.92" };

		BigDecimal bd1 = new BigDecimal("7676868683452352345324534534523453245234523452345234523452345");
		System.out.println(bd1);

		BigInteger bi1 = new BigInteger("7676868683452352345324534534523453245234523452345234523452345");
		System.out.println(bi1);

		String st3 = "Berlin";

		if (st3.equals("Milano")) {
			System.out.println("Ilm on soe.");
		} else {
			System.out.println("Ilm polegi k�ige t�htsam");
		}

		int i1 = 3;
		if (i1 == 5) {
			System.out.println("V�ga hea");
		} else if (i1 == 4) {
			System.out.println("Hea");
		} else if (i1 == 3) {
			System.out.println("Rahuldav");
		} else if (i1 == 2) {
			System.out.println("Mitterahuldav");
		} else if (i1 == 1) {
			System.out.println("N�rk");
		} else {
			System.out.println("See pole �ige hinne");
		}

		System.out.println("Hello, World!");
		System.out.println("Hello, 'World!'");
		System.out.println("Hello, \"World\"!");
		System.out.println("Steven Hawking once said: \"Life would be tragic if it weren't funny\"");
		System.out.println(
				"Kui liita kokku s�ned \"See on teksti esimene pool\" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\"");

		String tallinnPopulation = "450 000";
		System.out.println("Tallinnas elab " + tallinnPopulation + " inimest");

		int populationOfTallinn = 450000;
		System.out.println("Tallinnas elab " + populationOfTallinn + " inimest");

		String bookTitle = "Rehepapp";
		System.out.println("Raamatu \"" + bookTitle + "\" autor on Andrus Kivir�hk");

		String planet1 = "Merkuur";
		String planet2 = "Venus";
		String planet3 = "Maa";
		String planet4 = "Marss";
		String planet5 = "Jupiter";
		String planet6 = "Saturn";
		String planet7 = "Uran";
		String planet8 = "Neptuun";
		String planetCount = "8";
		System.out.println(planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " + planet5 + ", " + planet6
				+ ", " + planet7 + " ja " + planet8 + " on P�ikeses�steemi " + planetCount + " planeeti");

		System.out.println(String.format("%s, %s, %s, %s, %s, %s, %s ja %s on P�ikeses�steemi %s planeeti.", planet1,
				planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount));

	}

}
