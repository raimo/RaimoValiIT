package valiit;

// import java.math.BigDecimal;

public class App {
	public static void main(String[] args) {
		// System.out.println("Hello " + args[1] + "!");
		/*
		 * byte b1 = 2; byte b2 = 3; short b3 = (byte) (b1 + b2);
		 * System.out.println(b3);
		 * 
		 * float f1 = 1.1f; float f2 = 2.2f; double f3 = f1 + f2;
		 * System.out.println(f3);
		 * 
		 * BigDecimal bd1 = new BigDecimal(
		 * "23234548.000000000000000000000000000000000000000000000000000000000000000000001"
		 * ); BigDecimal bd2 = new BigDecimal("2"); BigDecimal bd3 = bd1.multiply(bd2);
		 * System.out.println(bd3);
		 * 
		 * boolean bo1 = false; boolean bo2 = true; boolean bo3 = bo1 && bo2;
		 * 
		 * System.out.println(bo3);
		 * 
		 * char ch1 = 'a'; char ch2 = 174; System.out.println((short) ch1);
		 * System.out.println(ch2);
		 */
		// Integer i = 5;
		// Integer i2 = new Integer(5);
		// int j = 5;
		// Integer i3 = i + i2;
		// System.out.println(i3);

		// int a = 4;
		// boolean test = a == 4;
		// System.out.println(test);
		//
		// int i = 1;
		// int i2 = i++; // 3 enne j�rgmist operatsiooni suurendatakse muutuja v��rtust
		// �he v�rra
		// System.out.println(i);
		// System.out.println(i2);
		// int j1 = 10;
		// int j2 = --j1; // 3 peale j�rgmist operatsiooni suurendatakse muutuja
		// v��rtust �he
		// System.out.println(j1);
		// System.out.println(j2);

		// i = i + 1; // 2
		// i++;
		// i--; // 2
		// ++i;
		// v�rra
		// --i; // 2
		// int u = 5;
		// u = u + 8;
		// u += 8; // suurenda u v��rtust 8 v�rra
		//
		// u = u - 3;
		// u -= 3; // v�henda u v��rtust 3 v�rra
		//
		// int i5 = 8;
		// System.out.println(i5);
		// i5 = 3 * i5;
		// System.out.println(i5);
		//
		// i5 = i5/13;
		// System.out.println(i5); // kui jagamisel tekib komaga arv, siis komakoha
		// numbrid visatakse �ra
		//
		// double d5 =i5/ 7d;
		// System.out.println(d5);

		// boolean b1 = true;
		// boolean b2 = false;
		// boolean b3 = !b2; // h��m�rk p��rab t�e v��rtuse vastupidiseks
		// boolean b4 = (b1 && b2) || b3;
		// System.out.println(b4);

		//int i = 10;
		int u =11 % 3;
		System.out.println(u);

	}

}
