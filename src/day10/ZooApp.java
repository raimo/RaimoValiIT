package day10;

public class ZooApp {

	public static void main(String[] args) {

		if (!Visitors.importVisitorData(args[0])) {
			return;
		}
		System.out.println(Visitors.getMaxVisitorDay());
	}

}
