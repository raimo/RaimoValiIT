package day10;

import java.util.Comparator;

public class VisitorComparator implements Comparator<Visitors> {

	@Override
	public int compare(Visitors o1, Visitors o2) {
		return o1.getVisitorCount() - o2.getVisitorCount();
	}

}
