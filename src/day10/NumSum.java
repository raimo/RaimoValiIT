package day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class NumSum {

	public static void main(String[] args) throws IOException {

		List<String> numbers = new ArrayList<>();

		Path path = Paths.get(args[0]);
		List<String> fileLines;
		fileLines = Files.readAllLines(path);
		for (String fileLine : fileLines) {
			numbers.add(fileLine);
		}

		String finalNumber = "";
		int tempNum = 0;
		for (int j = numbers.get(1).length() - 1; j >= 0; j--) {
			for (int i = 0; i <= numbers.size() - 1; i++) {
				int x = Integer.parseInt(numbers.get(i).substring(j, j + 1));
				tempNum = tempNum + x;
			}
			int reminder = tempNum / 10;
			int digit = tempNum % 10;
			finalNumber = Integer.toString(digit) + finalNumber;
			tempNum = reminder;
		}
		finalNumber = Integer.toString(tempNum) + finalNumber;
		System.out.println(finalNumber);
	}
}
