package day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Visitors {

	private String date;
	private int visitorCount;
	private static List<Visitors> visitors = new ArrayList<>();

	public Visitors(String date, int visitorCount) {
		this.date = date;
		this.visitorCount = visitorCount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getVisitorCount() {
		return visitorCount;
	}

	public void setVisitorCount(int visitorCount) {
		this.visitorCount = visitorCount;
	}

	public static boolean importVisitorData(String filePath) {

		try {
			Path path = Paths.get(filePath);
			List<String> fileLines;
			fileLines = Files.readAllLines(path);
			for (String fileLine : fileLines) {
				String[] lineParts = fileLine.split(", ");
				visitors.add(new Visitors(lineParts[0], Integer.parseInt(lineParts[1])));
			}

			visitors.sort(new VisitorComparator());
			System.out.println(visitors);

			return true;
		} catch (IOException e) {
			System.out.println("Error. Could not find the specified file.");
			return false;
		}
	}
 
	public static String getMaxVisitorDay() {
		int x = visitors.size() - 1;
		return "Maximum visitor count was " + visitors.get(x).getVisitorCount() + " on " + visitors.get(x).date;
	}

	@Override
	public String toString() {
		return String.format("\n%s Visitors: %s", this.getDate(), this.getVisitorCount());
	}

}
