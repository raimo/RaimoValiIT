package day04;

public class Cycles {

	public static void main(String[] args) {

		// BREAK & CONTINUE

		// for (int i = 1; i < 10000; i++) {
		// if (i < 650) {
		// System.out.println("L�petame selle ts�kli tegevuse.");
		// continue; // j�tkame uue ts�kliga
		// }
		//
		// if (i % 650 == 0) {
		// System.out.println(i);
		// break; // l�petame ts�kli tegevuse
		// }
		// }

		// �lesanne 11
		int i = 1;

		while (i < 101) {
			System.out.println(i++);

		}

		// �lesanne 12
		System.out.println(" ");
		for (int j = 1; j < 101; j++) {
			System.out.println(j);
		}

		// �lesanne 13
		System.out.println(" ");
		int[] m = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		for (int x : m) {
			System.out.println(x);
		}

		// �lesanne 14
		System.out.println(" ");
		for (int j = 1; j < 101; j++) {
			if (j % 3 == 0) {
				System.out.println(j);
			}
		}

		// �lesanne 15
		System.out.println(" ");
		String[] band = { "Sun", "Metsat�ll", "Queen", "Metallica" };
		for (i = 0; i < band.length; i++) {
			if (i < band.length - 1)
				System.out.print(band[i] + ", ");
			else
				System.out.println(band[i]);
		}
		// �lesanne 16
		System.out.println(" ");
		for (i = band.length - 1; i > -1; i--) {
			if (i > 0)
				System.out.print(band[i] + ", ");
			else
				System.out.println(band[i]);
		}

		// �lesanne 17
		System.out.println(" ");

		int pos = 0;

		for (String num : args) {
			int n = Integer.parseInt(num);
			if (pos < args.length - 1) {
				writeNumber(n);
				System.out.print(", ");
				pos++;
			} else {
				writeNumber(n);
			}
			System.out.println(" ");
		}

		// �lesanne 18 - DO WHILE
		System.out.println(" ");

		Double nmbr = Math.random();

		do { 
			System.out.println("Tere, " + nmbr);
			nmbr= Math.random();
		}
		while (nmbr>0.5); 
			
		
	}

	private static void writeNumber(int num) {
		switch (num) {
		case 9:
			System.out.print("�heksa");
			break;
		case 8:
			System.out.print("kaheksa");
			break;
		case 7:
			System.out.print("seitse");
			break;
		case 6:
			System.out.print("kuus");
			break;
		case 5:
			System.out.print("viis");
			break;
		case 4:
			System.out.print("neli");
			break;
		case 3:
			System.out.print("kolm");
			break;
		case 2:
			System.out.print("kaks");
			break;
		case 1:
			System.out.print("�ks");
		case 0:
			System.out.print("null");
			break;
		default:
			System.out.println("Critical malfunction");
		}
	}

}
