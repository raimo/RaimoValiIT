package day04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Kollektsioonid {

	public static void main(String[] args) {

		// �LESANNE 19
		List<String> cities = new ArrayList<>(Arrays.asList("Tallinn", "Tartu", "Rakvere", "P�rnu", "Narva"));
		System.out.println(cities.get(0) + ", " + cities.get(2) + ", " + cities.get(cities.size() - 1));

		// �LESANNE 20

		LinkedList<String> names = new LinkedList<>(
				Arrays.asList("Mihkel", "Joosep", "Kadri", "Malle", "Juku", "Mari"));
		System.out.println(names);

		while (!names.isEmpty()) { // kui list ei ole t�hi tee see tegevus
			System.out.println(names.remove()); // kustutab �he elemendi listist
		}
		System.out.println(names);

		// �LESANNE 21

		Set<String> set1 = new TreeSet<>(Arrays.asList("nimi3", "nimi2", "nimi1", "nimi4"));
		set1.forEach(x -> {
			System.out.println(x);
		});

		// �LESANNE 22

		Map<String, String[]> cntry = new HashMap<>();
		cntry.put("Estonia", new String[] { "Tallinn", "Tartu", "Valga", "V�ru" });
		cntry.put("Sweden", new String[] { "Stockholm", "Uppsala", "Lund", "K�ping" });
		cntry.put("Finland", new String[] { "Helsinki", "Espoo", "Hanko", "J�ms�" });

		// printout variant 1
		for (String key : cntry.keySet()) {
			System.out.println("Country: " + key);
			System.out.println("Cities:");
			for (String city : cntry.get(key)) {
				System.out.println("    "+ city);
			}
			System.out.println();
		}
		// printout variant 2
		
		cntry.forEach((countryName, cityNames)-> {
			System.out.println("Country: " + countryName);
			System.out.println("Cities:");
			for (String cityName : cityNames) {
				System.out.println("    "+ cityName);
			}
			System.out.println();
				});
		
	}

}
