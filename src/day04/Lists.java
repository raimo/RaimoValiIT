package day04;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Lists {

	public static void main(String[] args) {
		int[] arr1 = { 5, 7, 2, 4, 6 };
		System.out.println(arr1[2]);
		arr1[2] = 98;
		System.out.println(arr1[2]);
		System.out.println(arr1.length);
		
		// List<Integer> list1 = new ArrayList<>(Arrays.asList(5,7,2,4,6));
		// list1.add(5); // �kshaaval andmete lisamine
		// list1.add(7);
		// list1.add(2);
		// list1.add(4);
		// list1.add(6);
		
		List<Integer> list1 = new ArrayList<>(Arrays.asList(5, 7, 2, 4, 6));
		list1.add(958); // by default lisab k�ige l�ppu v��rtuse
		list1.add(0, 100); // lisab uue v��rtuse kohale 0 ja l�kkab k�ik teised edasi
		list1.add(2, 100);
		System.out.println(list1);

		System.out.println(list1.get(3));
		list1.remove(0); // remove k�sklusesse lisada indeks
		System.out.println(list1);
		list1.removeIf((x) -> {
			return x > 5;
		});
		// list1.removeIf(x -> x>5); //l�hem variant eelmisest. Eemaldab k�ik v��rtused
		// mis on suuremad kui 5

		System.out.println(list1);

		System.out.println(list1.size());

		list1.forEach(x -> {
			System.out.println("Prindin v�lja elemendi...");
			System.out.println("   Elemendi v��rtus on " + x);
			System.out.println("   Elemendi v��rtuse kasv on " + (x + 1));
			System.out.println("---------");
		});

		Set<String> set1 = new HashSet<String>(Arrays.asList("viis", "kuus", "seitse"));
		System.out.println(set1);
//		set1.add("seitse"); // ei lisa seda, sest on juba olemas nimekirjas
//		set1.add("kaheksa");
//		System.out.println(set1);
//
//		String[] elementsFromSet = set1.toArray(new String[0]);
//		System.out.println(elementsFromSet[2]);
//
//		set1.remove("viis");
//		System.out.println(set1);
//		set1.removeIf(x -> x.equals("viis") || x.equals("kuus"));
//		
//		System.out.println(set1);
		
		Set <String> set2 = new TreeSet <>(Arrays.asList("viis", "kuus", "seitse"));
		System.out.println(set2); //sorteerib t�hestiku j�rjekorras
		
		Set<String> set3= new TreeSet <>(new Comparator <String>() {
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}
		});
		set3.addAll(Arrays.asList("viis", "kuus", "seitse"));
		System.out.println(set3);
		
		Map<String, String> map1 = new HashMap<>();
		map1.put("auto", "car");
		map1.put("�ks", "one");
		map1.put("maja", "house");
		System.out.println(map1);
		System.out.println(map1.get("maja"));
		map1.remove("maja");
		System.out.println(map1);
		System.out.println(map1.keySet());
		System.out.println(map1.values());
		map1.forEach(
				(a,b) -> {
					System.out.print(a = a + " key ");
					System.out.println(b="value = " + b);
				});
		for( String key: map1.keySet()) {
			System.out.println("KEY: " + key + " = " + map1.get(key));
		}
		
		
		
//		Map<String, List<String>> map2 = new HashMap<>();
//		map2.put("�ks", Arrays.asList("one", "two", "three"));
//		
//		}
		// �LESANNE 19
		
		
	}

}
