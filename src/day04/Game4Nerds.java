package day04;

import java.util.Random;
import java.util.Scanner;

public class Game4Nerds {

	public static void main(String[] args) {

		System.out.println("Paku number vahemikus 1-10 000.");
		Scanner inputScanner = new Scanner(System.in);
		String decision = null;

		do {
			play(inputScanner);
			System.out.println("Kas soovid uuesti m�ngida? y/n");
			decision = inputScanner.next();
		} while (decision.equals("y"));

		System.out.println("That's my game, NERD!");

	}

	private static void play(Scanner inputScanner) {

		int randomNumber = new Random().nextInt(10000) + 1;
		int counter = 1;
		int userGuess = 0;

		do {
			System.out.println("Sinu pakkumine: ");
			userGuess = inputScanner.nextInt();
			if (userGuess == randomNumber) {
				System.out.println("�ige, sul kulus " + counter + " korda, et �ra arvata! What a NERD!");
			} else if (userGuess < randomNumber) {
				System.out.println("Liiga v�ike, NERD!");
				counter++;
			} else {
				System.out.println("Liiga suur, NERD!");
				counter++;
			}
		} while (userGuess != randomNumber);
	}
}
