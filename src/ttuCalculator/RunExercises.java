package ttuCalculator;

import ttuCalculator.Calculator.LongerPad;

public class RunExercises {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		System.out.println(calculator.convertName("Casio"));
		System.out.println(calculator.addition(3, 5));
		System.out.println(calculator.subtraction(9, 4));
		System.out.println(calculator.repeat("Kaunis tekst.", 5));
		System.out.println(calculator.line(25, true));
		System.out.println(calculator.line2(25));
		System.out.println(calculator.center("123", 6, LongerPad.RIGHT));
		
	} 

} 
