package ttuCalculator;

public class ConvertName {
	private static String convertName(String s) {
		int textLength = s.length();

		if (textLength >= 3) {
			String firstThreeLetters = s.substring(0, 2).toUpperCase();
			String textLengthNumber = Integer.toString(textLength);
			String lastTwoLetters = s.substring(textLength - 2, textLength - 1).toLowerCase();
			return firstThreeLetters + textLengthNumber + lastTwoLetters;
		}
		return "ERROR";
	}
}
