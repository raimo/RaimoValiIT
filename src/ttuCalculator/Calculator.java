package ttuCalculator;

public class Calculator {

	public static void main(String[] args) {
		System.out.println(convertName("Burroughs")); // "BUR-9hs"
		System.out.println(convertName("abc")); // "ABC-3bc"
		System.out.println(convertName("")); // "ERROR"
		System.out.println(addition(2, 3)); // "2 + 3 = 5
		System.out.println(repeat("a", 3)); // "aaa"
		System.out.println(line2(4)); // "----"
		System.out.println(line(4, true)); // ">--<"

		// bonus
		System.out.println(center("ab", 5, LongerPad.LEFT)); // " ab "
		System.out.println(center("ab", 5, LongerPad.RIGHT)); // " ab "
		System.out.println(center("bacon", 3, LongerPad.LEFT)); // "aco"
		System.out.println(center("abcde", 2, LongerPad.LEFT));
		// System.out.println(display(2, 3, "abc", "addition", 20));
		/*
		 * ABC-3bc >------------------< | 2 + 3 = 5 | --------------------
		 */
		// System.out.println(display(1, 1, "cheese", "z", 1));
		/*
		 * CHE-6se >-----< |ERROR| -------
		 */
	}

	public static enum LongerPad {
		LEFT, RIGHT
	}

	/**
	 * 
	 * Return a string following a naming convention.
	 * 
	 * [first three letters in uppercase]-[length of string][last two letters of
	 * string in lowercase]
	 * 
	 * If length of string is less than 3, return "ERROR".
	 *
	 * 
	 * 
	 * @param s
	 *            original name
	 * 
	 */

	public static String convertName(String s) {
		int textLength = s.length();

		if (textLength >= 3) {
			String firstThreeLetters = s.substring(0, 3).toUpperCase();
			String textLengthNumber = Integer.toString(textLength);
			String lastTwoLetters = s.substring(textLength - 2, textLength).toLowerCase();
			return firstThreeLetters + "-" +textLengthNumber + lastTwoLetters;
		}
		return "ERROR";

	}

	/**
	 * 
	 * Return an expression that sums the numbers a and b.
	 * 
	 * Example: a = 3, b = 7 -> "3 + 7 = 10"
	 * 
	 */

	public static String addition(int a, int b) {
		int sum = a + b;
		return a + " + " + b + " = " + sum;

	}

	/**
	 * 
	 * Return an expression that subtracts b from a.
	 * 
	 * Example: a = 3, b = 1 -> "3 - 1 = 2"
	 * 
	 */

	public String subtraction(int a, int b) {
		int sum = a - b;
		return a + " - " + b + " = " + sum;

	}

	/**
	 * 
	 * Repeat the input string n times.
	 * 
	 */

	public static String repeat(String s, int n) {
		for (int i = 0; i < n; i++) {
			System.out.print(s + "");
			;
		}
		return "";

	}

	/**
	 * 
	 * Create a line separator using "-". Width includes the decorators if it has
	 * any.
	 *
	 * 
	 * 
	 * @param width
	 *            width of the line, which includes the decorator, if it has one
	 * 
	 * @param decorated
	 *            if True, line starts with ">" and ends with "<", if False,
	 *            consists of only "-"
	 * 
	 *            If decorated and width is 1, return an empty string ("").
	 * 
	 */

	public static String line(int width, boolean decorated) {
		if (decorated && width != 1) {
			System.out.print(">");
			for (int i = 0; i < width; i++) {
				System.out.print(" - ");
			}
			System.out.println("<");
		} else if (!decorated) {
			for (int i = 0; i < width - 1; i++) {
				System.out.print("-");
			}
			System.out.println("-");
		} else {
			System.out.println("");
			;
		}
		return "";
	}

	/**
	 * 
	 * Create a line separator using "-".
	 *
	 * 
	 * 
	 * @param width
	 *            width of the line.
	 * 
	 */

	public static String line2(int width) {
		for (int i = 0; i < width - 1; i++) {
			System.out.print(" - ");
		}
		System.out.println(" - ");
		return "";

	}

	/**
	 * 
	 * Center justify a string.
	 *
	 * "a", 3, LongerPad.LEFT -> " a "
	 *
	 * When the string does not fit exactly:
	 * 
	 * If LongerPad.LEFT make the left padding longer, otherwise the right padding
	 * should be longer.
	 * 
	 * "ab", 5, LongerPad.LEFT -> " ab " "ab", 5, LongerPad.RIGHT -> " ab "
	 *
	 * If the width is smaller than the length of the string, return only the center
	 * part of the text.
	 * 
	 * "abcde", 2, LongerPad.LEFT -> "bc" or "cd" (both are fine)
	 *
	 * Return an empty string ("") if the width is negative.
	 * 
	 * @param s
	 *            string to center
	 * @param width
	 *            width of the outcome
	 * @param pad
	 *            left longer if LongerPad.LEFT, pad right longer if LongerPad.RIGHT
	 * @return new center justified string
	 */

	private static void looper1(String s, int width) {
		int sub = (width - s.length()) / 2;
		for (int x1 = 0; x1 < sub; x1++) {
			System.out.print(" ");
		}
		System.out.print(s);
		for (int x1 = 0; x1 < sub; x1++) {
			System.out.print(" ");
		}

	}

	private static void looper2(String s, int width) {
		int sub = (width - s.length()) / 2 + 1;
		int sub2 = (width - s.length()) / 2;
		for (int x1 = 0; x1 < sub; x1++) {
			System.out.print(" ");
		}
		System.out.print(s);
		for (int x1 = 0; x1 < sub2; x1++) {
			System.out.print(" ");
		}
	}

	private static void looper3(String s, int width) {
		int sub = (width - s.length()) / 2;
		int sub2 = (width - s.length()) / 2 + 1;
		for (int x1 = 0; x1 < sub; x1++) {
			System.out.print(" ");
		}
		System.out.print(s);
		for (int x1 = 0; x1 < sub2; x1++) {
			System.out.print(" ");
		}
	}

	public static String center(String s, int width, LongerPad pad) {
		// char[] letters = s.toCharArray();
		int sub = width - s.length();

		if (width < 0) {
			System.out.println("");
		} else if (width >= s.length()) {
			if (sub % 2 == 0) {
				looper1(s, width);
			} else {
				switch (pad) {
				case LEFT:
					looper2(s, width);
					break;
				case RIGHT:
					looper3(s, width);
					break;
				}
			}
		} else {
			int begin = s.length() / 2 - width / 2;
			System.out.println(s.substring(begin, begin + width));
		}
		return "";

	}

	/**
	 * 
	 * Create a string representing the display.
	 * 
	 * Use LongerPad.LEFT when centering.
	 * 
	 * Width of the display must be set to the assigned width or expression width,
	 * whichever is bigger.
	 * 
	 * If the operation is not valid, display "ERROR".
	 * 
	 * @param a
	 *            number
	 * @param b
	 *            number
	 * @param name
	 *            full name of calculator company
	 * @param operation
	 *            operation ("addition" or "subtraction") applied to numbers a and b
	 * @param width
	 *            width of the display
	 * @return string representing the final format
	 */

	// private static String display(int a, int b, String name, String operation,
	// int width) {
	// if ()
	// return "";

}
