package ylesanded;

public class Ylesanded {

	public static void main(String[] args) {

		// �lesanne 2 - valikute konstrueerimine
		String st3 = "Berlin";
		doExercise2(st3);

		// �lesanne 3: if, else if, else
		int i1 = 4;
		doExercise3(i1);
 
		// �lesanne 4: switch
		doExercise4(i1);

		// �lesanne 5: inline-if
		int vanus = 100;
		doExercise5(vanus); 

		// �lesanne 6: inline-if
		doExercise6(vanus);
		
		test(2);
		
		test2("tere", "head aega");
	}


	private static void doExercise2(String city) {
		if (city.equalsIgnoreCase("Milano")) {
			System.out.println("Ilm on soe.");
		} else {
			System.out.println("Ilm polegi k�ige t�htsam");
		}

	}

	private static void doExercise3(int mark) {
		if (mark == 5) {
			System.out.println("V�ga hea");
		} else if (mark == 4) {
			System.out.println("Hea");
		} else if (mark == 3) {
			System.out.println("Rahuldav");
		} else if (mark == 2) {
			System.out.println("Mitterahuldav");
		} else if (mark == 1) {
			System.out.println("N�rk");
		} else {
			System.out.println("See pole �ige hinne");
		}
	}

	private static void doExercise4(int mark) {
		switch (mark) {
		case 5:
			System.out.println("V�ga hea");
			break;
		case 4:
			System.out.println("Hea");
			break;
		case 3:
			System.out.println("Rahuldav");
			break;
		case 2:
			System.out.println("Mitterahuldav");
			break;
		case 1:
			System.out.println("N�rk");
			break;
		default:
			System.out.println("See pole �ige hinne");
		}
	}

	static void doExercise5(int age) {
		System.out.println((age > 100) ? "Vana" : "Noor");
	}

	static void doExercise6(int age) {
		String kasOnVana2 = (age == 100) ? "Peaaegu vana" : (age < 100) ? "Noor" : "Vana";
		System.out.println(kasOnVana2);
	}
	
	private static void test (int numb) {
		boolean b = true;
		System.out.println(b);
	}
	
	private static void test2 (String ac, String ab) {
		
	}
}

