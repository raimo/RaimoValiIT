package day08;

public class drunkIrishDog extends Dog {

	@Override
	public void bark(int barkCount) {
		System.out.print("Drunk Irish dog: ");
		for (int i = 0; i < barkCount - 1; i++) {
			System.out.print("Amh - amh! ");
		}
		System.out.println("Amh - amh!");
	}

}
