package day08;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppTest {

	@BeforeClass //k�ige esimene meetod, mis k�ivitatakse enne k�iki uniteid, nt seab �les �henduse andmebaasiga. K�ivitatakse igal juhul, olenemata kas test �nnestub v�i mitte
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass // k�ige viimane meetod. K�ivitatakse igal juhul, olenemata kas test �nnestub v�i mitte
	public static void tearDownAfterClass() throws Exception {
	}

	@Before // K�ivitatakse enne igat testmeetodi k�ivitamist. 
	public void setUp() throws Exception {
	}

	@After // K�ivitatakse peale igat testmeetodi k�ivitamist. 
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddNumbers() {
		int result = App.addNUmbers(2, 2);
		assertTrue(result == 4);
		assertEquals(4, 4);
	}
	
	@Test
	public void testPerson() {
		boolean result = day06.MethodsExercises.validatePersonalCode(new BigInteger("38807244911"));
		assertTrue(result);
		
		result = day06.MethodsExercises.validatePersonalCode(null);
		assertFalse(result);
		
		result = day06.MethodsExercises.validatePersonalCode(new BigInteger("135864"));
		assertFalse(result);
	}
	
	@Test
	public void testIsPersonalCodeCorrect() {
		boolean result = day06.MethodsExercises.isPersonalCodeCorrect("38104242729");
		assertTrue(result);
		
		result = day06.MethodsExercises.isPersonalCodeCorrect("38145242728");
		assertFalse(result);
		
		result = day06.MethodsExercises.isPersonalCodeCorrect("38104782721");
		assertFalse(result);
		
		result = day06.MethodsExercises.isPersonalCodeCorrect("78104242722");
		assertFalse(result);
	}

	@Test
	public void testDeriveGender () {
		assertTrue(day06.MethodsExercises.deriveGender("38807244911").equals("M"));
		assertTrue(day06.MethodsExercises.deriveGender("48807244911").equals("F"));
	}

}
