package day08;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class CryptorTest {

	@Test
	public void testDateBasedEncryptor() throws IOException {
		Cryptor encryptor = new DateBasedEncryptor("C:/Users/opilane/Desktop/Vali IT/Programmi failid/alfabeet2.txt");
		String inputText = "ABCDEFGHIJKLMNOPQRS�Z�TUVW����XYabcdefghijklmnopqrs�z�tuvw����xy .,!:;-+";
		String outputText = "2105;2106;2107;2108;2109;2110;2111;2112;2113;2114;2115;2116;2117;2118;2119;2120;2121;2122;2123;2392;2130;2421;2124;2125;2126;2127;2253;2236;2254;2260;2128;2129;2137;2138;2139;2140;2141;2142;2143;2144;2145;2146;2147;2148;2149;2150;2151;2152;2153;2154;2155;2393;2162;2422;2156;2157;2158;2159;2285;2268;2286;2292;2160;2161;2072;2086;2084;2073;2098;2099;2085;2083;";
		assertTrue(encryptor.translate(inputText).equals(outputText));
	}

	@Test
	public void testDateBasedDecryptor() throws IOException {
		Cryptor decryptor = new DateBasedDecryptor("C:/Users/opilane/Desktop/Vali IT/Programmi failid/alfabeet2.txt");
		String inputText = "ABCDEFGHIJKLMNOPQRS�Z�TUVW����XYabcdefghijklmnopqrs�z�tuvw����xy .,!:;-+";
		String outputText = "2105;2106;2107;2108;2109;2110;2111;2112;2113;2114;2115;2116;2117;2118;2119;2120;2121;2122;2123;2392;2130;2421;2124;2125;2126;2127;2253;2236;2254;2260;2128;2129;2137;2138;2139;2140;2141;2142;2143;2144;2145;2146;2147;2148;2149;2150;2151;2152;2153;2154;2155;2393;2162;2422;2156;2157;2158;2159;2285;2268;2286;2292;2160;2161;2072;2086;2084;2073;2098;2099;2085;2083;";
		assertTrue(decryptor.translate(outputText).equals(inputText));
	}

}