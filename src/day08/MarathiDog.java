package day08;

public class MarathiDog extends Dog {
	@Override
	public void bark(int barkCount) {
		System.out.print("Marathi dog: ");
		for (int i = 0; i < barkCount - 1; i++) {
			System.out.print("Bhu - bhu! ");
		}
		System.out.println("Bhu - bhu!");
	}
}
