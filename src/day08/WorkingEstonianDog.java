package day08;

public class WorkingEstonianDog extends Dog {
	public void bark(int barkCount) {
		System.out.print("Working Estonian dog: ");
		for (int i = 0; i < barkCount - 1; i++) {
			System.out.print("Auh - auh! ");
		}
		System.out.println("Auh - auh!");
	}
}
