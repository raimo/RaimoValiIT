package day08;

import java.util.Objects;

public abstract class Car implements Movable, Comparable<Car> {

	private String color;
	private int maxSpeed;
	private int seatCount;
 
	@Override
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String getColor() {
		return this.color;
	}

	@Override
	public void setMaxSpeed(int speed) {
		this.maxSpeed = speed;

	}

	@Override
	public int getMaxSpeed() {

		return this.maxSpeed;
	}

	@Override
	public void setSeatCount(int seatCount) {
		this.seatCount = seatCount;
	}

	@Override
	public int getSeatCount() {

		return this.seatCount;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) { // v�rdleb objekti iseendaga. Kui on v�rdne annab TRUE
			return true;
		}
		if (obj == null) { // kontrollib kas objekt on mitte midagi
			return false;
		}
		if (this.getClass() != obj.getClass()) { // kontrollib objektide klassi
			return false;
		}
		Car otherCar = (Car) obj; // objekti v�rdlemine teise loogiliselt samav��rse objektiga
		boolean colorSame = this.getColor().equals(otherCar.getColor());
		boolean speedSame = this.getMaxSpeed() == otherCar.getMaxSpeed();
		boolean seatCountSame = this.getSeatCount() == otherCar.getSeatCount();

		return colorSame && speedSame && seatCountSame;
	}

	@Override
	public int hashCode() { // muudan hashcode'i panemise meetodit, siis on samade omadustega objektil sama
							// hashCode
		int colorHashCode = Objects.hash(this.getColor());
		int maxSpeedHashCode = ((Integer) this.getMaxSpeed()).hashCode();
		int seatCountHashCode = ((Integer) this.getSeatCount()).hashCode();

		return Math.abs(7 + 24 * colorHashCode + maxSpeedHashCode * seatCountHashCode);

	}

	@Override
	public int compareTo(Car o) {
		
		return this.getSeatCount() - o.getSeatCount();

	}
	
	
	
	
	
	
	
	
	
	
	
}
