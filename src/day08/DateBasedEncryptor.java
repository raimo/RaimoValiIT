package day08;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateBasedEncryptor extends Cryptor {

	public DateBasedEncryptor(String filePath) throws IOException {
		super(filePath);
	}

	@Override
	protected Map<String, String> generateDictionary(List<String> fileLines) {
		Map<String, String> dictionary = new HashMap<>();

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH)+1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int tmpNum = year + month + day;

		for (String dictLine : fileLines) {
			char letterToChar = dictLine.toCharArray()[0];
			int charToNum = (int)letterToChar;
			int encryptNum = charToNum + tmpNum;
			String encryptedChar = Integer.toString(encryptNum)+";";
			dictionary.put(dictLine, encryptedChar);
		}
		return dictionary;
	}

	@Override
	public String translate(String text) {
		if (text != null) {
			String translatedText = "";
			char[] textChars = text.toCharArray();
			for (char c : textChars) {
				String letter = String.valueOf(c);
				translatedText = translatedText + this.dictionary.get(letter);
			}
			return translatedText;
		}
		return null;
	}

}
