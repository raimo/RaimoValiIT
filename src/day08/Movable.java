package day08;

public interface Movable {

	void setColor(String color);
	String getColor();
	void setMaxSpeed(int speed);
	int getMaxSpeed();
	
	void setSeatCount(int seatCount);
	int getSeatCount();
	
	
	void drive();

}
