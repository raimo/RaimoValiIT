package day06;

public class Person {

	private String personCode;

	public Person(String personCode) { // antud tase on meetodi tase ning siia antud v��rtused kaovad, kui meetod
										// l�petab tegevuse.
		this.personCode = personCode; // p��rdub klassi muutuja poole, algv��rtustab klassi parameetreid. Anname
										// meetodi parameetri v��rtuse edasi objektile parameetrile

	}

	public int getBirthYear() {
		if (personCode != null && personCode.length() == 11) {

			String yearNum = personCode.substring(1, 3);
			int year = Integer.parseInt(yearNum);
			String genderNum = personCode.substring(0, 1);
			int cent = Integer.parseInt(genderNum);
			if (cent == 1 || cent == 2) {
				return 1800 + year;
			} else if (cent == 3 || cent == 4) {
				return 1900 + year;
			} else if (cent == 5 || cent == 6) {
				return 2000 + year;
			}

		}
		return 0;
	}

	public String getBirthMonth() {
		if (personCode != null && personCode.length() == 11) {

			String monthNum = personCode.substring(3, 5);
			switch (monthNum) {
			case "01":
				return "Jaanuar";
			case "02":
				return "Veebruar";
			case "03":
				return "M�rts";
			case "04":
				return "Aprill";
			case "05":
				return "Mai";
			case "06":
				return "Juuni";
			case "07":
				return "Juuli";
			case "08":
				return "August";
			case "09":
				return "September";
			case "10":
				return "Oktoober";
			case "11":
				return "November";
			case "12":
				return "Detsember";
			}

		}
		return "Teadmata";
	}

	public String getBirthDayOfMonth() {
		if (personCode != null && personCode.length() == 11) {
			return personCode.substring(5, 7);

		}
		return null;
	}

	public Gender getPersonGender() {
		if (personCode != null) {
			int genderDeterminant = Integer.parseInt(personCode.substring(0, 1));
			if (genderDeterminant % 2 == 0) {
				return Gender.FEMALE;
			} else {
				return Gender.MALE;
			}
		}
		return Gender.UNKNOWN;
	}

}
