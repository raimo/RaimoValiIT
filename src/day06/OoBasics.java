package day06;

import java.util.Scanner;

public class OoBasics {

	public static void main(String[] args) {
		// OoBasics ooBasics = new Oobasics (); //l�in klassist objetki - objektil on
		// komponent millel on seisund ja omadused
		Dog dog = new Dog();
		// dog.name = "Muki";
		// dog.tailLength = 10;
		// System.out.println(dog.legCount);
		dog.bark(4);
		Dog dog2 = new Dog(); // iga objekti jaoks m��ratakse m�lus eraldi muutujad/omadused mis antud
								// objektile on antud
		// dog2.name = "Muri";
		Dog dog3 = new Dog();
		Dog dog4 = new Dog();

		Scanner scanner = new Scanner(System.in);

		Person person1 = new Person("38807244911");
		System.out.println(person1.getBirthYear());
		System.out.println(person1.getBirthMonth());
		System.out.println(person1.getBirthDayOfMonth());
		System.out.println(person1.getPersonGender());

		if (person1.getPersonGender() == Gender.MALE) {
			System.out.println("The person is male");
		} else if (person1.getPersonGender() == Gender.FEMALE) {
			System.out.println("The person is female");
		} else {
			System.out.println("The person is unknown.");
		}

		Dog.printDogCount();
		dog2.printFellowCount();

		dog.getAge();

	}

}
