package day06;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete o1, Athlete o2) {

		if (o1.getLastName().compareTo(o2.getLastName()) != 0) {
			return o1.getLastName().compareTo(o2.getLastName());
		} else if (o1.getFirstName().compareTo(o2.getFirstName()) != 0) {
			return o1.getFirstName().compareTo(o2.getFirstName());
		} else if (o1.getAge() < o2.getAge()) {
			return -1;
		} else if (o1.getAge() > o2.getAge()) {
			return 1;
		} else
			return 0;
		// if (o1.getAge() < o2.getAge()) {
		// return -1;
		// } else if (o1.getAge() > o2.getAge()) {
		// return 1;
		// }
		// return 0;

		// Kui o1 on v�iksem, kui o2, tuleb tagastada neg number
		// Kui 01 on suurem, kui o2, tagastada pos number
		// kui o1 ja o2 on v�rdsed, tagasta 0

	}

}
