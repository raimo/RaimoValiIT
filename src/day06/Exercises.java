package day06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Exercises {

	public static void main(String[] args) {
		// CountryInfo estonia = new CountryInfo();
		// estonia.name = "Estonia";
		// estonia.capital = "Tallinn";
		// estonia.primeMinister = "Jüri Ratas";
		// estonia.languages.addAll(Arrays.asList("Estonian", "Russian", "Finnish",
		// "Ukranian"));

		CountryInfo estonia = new CountryInfo("Estonia", "Tallinn", "Jüri Ratas",
				Arrays.asList("Estonian", "Russian", "Ukrainian", "Finnish"));

		CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Māris Kučinskis",
				Arrays.asList("Latvian", "Russian", "Ukrainian", "Polish"));
		// latvia.name = "Latvia";
		// latvia.capital = "Riga";
		// latvia.primeMinister = "Māris Kučinskis";
		// latvia.languages.add("Latvian");
		// latvia.languages.add("Russian");
		// latvia.languages.add("Polish");
		// latvia.languages.add("Lithuanian");
		// latvia.languages.add("Ukranian");

		List<CountryInfo> countries = new ArrayList<>();
		countries.add(estonia);
		countries.add(latvia);
		for (CountryInfo country : countries) {
			System.out.println(country);
		}

		// - - - - - - - - - - - - - - - -

		Runner myRunner1 = new Runner();
		myRunner1.setFirstName("Mihkel");
		myRunner1.setLastName("Mägi");
		myRunner1.setAge(45);
		myRunner1.setGender("M");
		myRunner1.setWeight(80.5d);
		myRunner1.setHeight(180);

		Runner myRunner2 = new Runner();
		myRunner2.setFirstName("Kristi");
		myRunner2.setLastName("Mets");
		myRunner2.setAge(39);
		myRunner2.setGender("F");
		myRunner2.setWeight(50.3d);
		myRunner2.setHeight(165);

		Runner myRunner3 = new Runner();
		myRunner3.setFirstName("Asko");
		myRunner3.setLastName("Mets");
		myRunner3.setAge(45);
		myRunner3.setGender("M");
		myRunner3.setWeight(72.0d);
		myRunner3.setHeight(185);

		Skydiver mySkydiver1 = new Skydiver();
		mySkydiver1.setFirstName("Kaupo");
		mySkydiver1.setLastName("Kägu");
		mySkydiver1.setAge(65);
		mySkydiver1.setGender("M");
		mySkydiver1.setWeight(130.9d);
		mySkydiver1.setHeight(202);

		Skydiver mySkydiver2 = new Skydiver();
		mySkydiver2.setFirstName("Kristi");
		mySkydiver2.setLastName("Mets");
		mySkydiver2.setAge(37);
		mySkydiver2.setGender("F");
		mySkydiver2.setWeight(68.7d);
		mySkydiver2.setHeight(170);

		Skydiver mySkydiver3 = new Skydiver();
		mySkydiver3.setFirstName("Mari");
		mySkydiver3.setLastName("Maias");
		mySkydiver3.setAge(20);
		mySkydiver3.setGender("F");
		mySkydiver3.setWeight(58.6d);
		mySkydiver2.setHeight(156);

		List<Athlete> athletes = new ArrayList<>();
		athletes.add(myRunner1);
		athletes.add(myRunner2);
		athletes.add(myRunner3);
		athletes.add(mySkydiver1);
		athletes.add(mySkydiver2);
		athletes.add(mySkydiver3);

		// athletes.sort(new AthleteComparator());
		athletes.sort(new Comparator<Athlete>() {

			@Override
			public int compare(Athlete o1, Athlete o2) {

				if (o1.getLastName().compareTo(o2.getLastName()) != 0) {
					return o1.getLastName().compareTo(o2.getLastName());
				} else if (o1.getFirstName().compareTo(o2.getFirstName()) != 0) {
					return o1.getFirstName().compareTo(o2.getFirstName());
				} else if (o1.getAge() < o2.getAge()) {
					return -1;
				} else if (o1.getAge() > o2.getAge()) {
					return 1;
				} else
					return 0;
				// if (o1.getAge() < o2.getAge()) {
				// return -1;
				// } else if (o1.getAge() > o2.getAge()) {
				// return 1;
				// }
				// return 0;

				// Kui o1 on väiksem, kui o2, tuleb tagastada neg number
				// Kui 01 on suurem, kui o2, tagastada pos number
				// kui o1 ja o2 on võrdsed, tagasta 0

			}
		});

		System.out.println(athletes);

		// Skydiver mySkydiver = new Skydiver();
		// Athlete mySkydiver2 = new Skydiver();
		// // Athlete myGenericAthlete = new Athlete();
		//
		// List<Athlete> athletes = new ArrayList<>();
		// athletes.add(myRunner);
		// athletes.add(mySkydiver);
		// // athletes.add(myGenericAthlete);
		// for (Athlete athlete : athletes) {
		// athlete.perform();
		// }

	}

}
