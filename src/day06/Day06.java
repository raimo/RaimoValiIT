package day06;

public class Day06 {

	public static void main(String[] args) {
		int i = 5;
		double pi = 3.14159d;
		double pi2 = getPi();
		int numberPow = doSomething(i);
		System.out.println(numberPow);
	}

	public static double getPi() { // kui ei tagasta v��rtust (void) siis ei saa otseselt n�iteks arvutamiseks
									// kasutada
		return 3.14159d;
	}

	public static int doSomething(final int someNumber) { // ei ole v�imalik sisendparameetrit muuta meetodi sees
		return someNumber * someNumber;
	}
}
