package day06;

import java.math.BigInteger;

public class MethodsExercises {

	public static void main(String[] args) {
		int i = 4;
		boolean isTrue = test(i);
		if (test(i)) {
			System.out.println("OK");
		}
		double x = 100d;
		System.out.println(addVAT(x));

		int[] xxxArr = xxx(3, 4, true);
		for (int n : xxxArr) {
			System.out.println(n);
		}

		System.out.println(deriveGender("38807244911"));
		System.out.println(deriveGender("48807244911"));

		printHello();

		System.out.println(deriveBirthYear("62507244911"));

		System.out.println(validatePersonalCode(new BigInteger("38807244911")));

		printOutNumbers(100);

	}

	// -----------------------------------------------------------

	static boolean test(int x) {
		if (x > 5) {
			return true;
		}
		return false;
	}

	static void test2(String text1, String text2) {

	}

	static double addVAT(double input) {
		double priceWithVat = 1.2d * input;
		return priceWithVat;
	}

	static int[] xxx(int n1, int n2, boolean t) {
		if (t) { // kui sisendparameeter t on t�ene, siis tagastab vastavad v��rtused
			return new int[] { n1 * n1, n2 * n2 };
		}

		return null; // seda saab teha ainult siis kui tagastust��p on objekt.
	}

	public static String deriveGender(String ik) {
		if (ik != null) {
			String genderNum = ik.substring(0, 1);
			int gender = Integer.parseInt(genderNum);
			if (gender % 2 == 0) {
				return "F";
			} else {
				return "M";
			}
		}
		return null;

	}

	static void printHello() {
		System.out.println("Tere");
	}

	static int deriveBirthYear(String ik) {
		if (ik != null) {

			String yearNum = ik.substring(1, 3);
			int year = Integer.parseInt(yearNum);
			String genderNum = ik.substring(0, 1);
			int cent = Integer.parseInt(genderNum);
			if (cent == 1 || cent == 2) {
				return 1800 + year;
			} else if (cent == 3 || cent == 4) {
				return 1900 + year;
			} else if (cent == 5 || cent == 6) {
				return 2000 + year;
			}

		}
		return 0;
	}

	// I astme kaal: 1 2 3 4 5 6 7 8 9 1
	// II astme kaal: 3 4 5 6 7 8 9 1 2 3

	public static boolean validatePersonalCode(BigInteger ik) {
		if (ik != null) {

			String personalCodeStr = ik.toString();
			if (personalCodeStr.length() == 11) {
				int tempSum = 0;
				int[] weights1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1 };
				for (int i = 0; i < weights1.length; i++) {
					int digit = Integer.parseInt(personalCodeStr.substring(i, i + 1));
					tempSum += digit * weights1[i]; // liidan varasemale v��rtusele juurde
				}
				int checkNumber = tempSum % 11;
				if (checkNumber != 10) {
					int personalCodeCheckNum = Integer.parseInt(personalCodeStr.substring(10, 11));
					return checkNumber == personalCodeCheckNum;
				} else {
					int[] weights2 = { 3, 4, 5, 6, 7, 8, 9, 1, 2, 3 };
					tempSum = 0;
					for (int i = 0; i < weights2.length; i++) {

						int digit = Integer.parseInt(personalCodeStr.substring(i, i + 1));
						tempSum += digit * weights2[i];
					}
					checkNumber = tempSum % 11;
					checkNumber = (checkNumber == 10) ? 0 : checkNumber; // kui tulemus on 10, siis muudame selle
																			// nulliks, �lej��nud juhtudel on
																			// checknumber l�plik number
					int personalCodeCheckNum = Integer.parseInt(personalCodeStr.substring(10, 11));
					return checkNumber == personalCodeCheckNum;
				}
			}

		}
		return false;
	}
 
	static int printOutNumbers(int i) {
		System.out.println(i);
		i = i - 1;
		if (i > 0) {
			return printOutNumbers(i);
		}
		return 0;
	}

	public static boolean isPersonalCodeCorrect(String ik) {
		boolean result = validatePersonalCode(new BigInteger(ik));
		if (result) {
			int month = Integer.parseInt(ik.substring(3, 5));
			result = month > 0 && month < 13;
		}

		int month = Integer.parseInt(ik.substring(3, 5));
		int day = Integer.parseInt(ik.substring(5, 7));
		if (result) {
			if (month == 2) {
				result = day > 0 && day <= 29;
			} else {
				switch (month) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					result = day > 0 && day < 32;
					break;
				default:
					result = day > 0 && day < 31;
				}
			}
		}

		if (result) {
			int firstDigit = Integer.parseInt(ik.substring(0, 1));
			result = firstDigit > 0 && firstDigit < 7;
		}
		return result;

	}
}
