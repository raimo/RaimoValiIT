package day06;

public abstract class Athlete { // abstract keelab �ra generic klassi v�lja printimise
	protected String firstName; // protected elemendid on n�htavad sama package sees
	protected String lastName;
	protected int age;
	protected String gender;
	protected int height; // �lej��nud maailm ei j��, aga alamklassid saavad ligi
	protected double weight;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public abstract void perform();

	protected void prepareForPerform() {
		// Valmistame sportlase ette sportimiseks

	}

	@Override
	public String toString() {

		return String.format("\n[fn=%s, ln=%s, a=%s, g=%s, w=%s, h=%s]", this.getFirstName(), this.getLastName(),
				this.getAge(), this.getGender(), this.getWeight(), this.getHeight());
	}
	// {
	// System.out.println("Default implementation. Please implement me in
	// subclass");
	// }
	//

	// public Athlete(String firstName, String lastName, int age, String gender, int
	// height, double weight) {
	// this.firstName = firstName;
	// this.lastName = lastName;
	// this.age = age;
	// this.gender = gender;
	// this.height = height;
	// this.weight = weight;
	// }

}
