package day06;

public class Dog {

	private int legCount = 4; // vaikimisi omadused/muutujud, mis kopeeritakse objektile kui see
								// luuakse(public)
	private String name = null;
	private int age = 0;
	public static int count = 0; // See (static) muutuja ei kuulu objektile. Sellest count muutujast ei tehta
									// objektile koopiat.

	private int tailLength = 0;

	public int getTailLength() {
		return tailLength;
	}

	public void setTailLength(int tailLength) {
		this.tailLength = tailLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLegCount() {
		return this.legCount;
	}

	public int getAge() {
		System.out.println("The dog " + this.name + " age was requested...");
		return this.age;
	}

	public void setAge(int dogAge) { // m��rab koera vanuse
		System.out.println("The dog " + this.name + " age was changed to " + dogAge);
		if (dogAge > 0 && dogAge < 25) {
			this.age = dogAge;
		} else {
			System.out.println("Invalid age setting attempt to " + this.name);
		}
	}

	public Dog() {
		Dog.count++; // iga kord kui koera objekt luuakse suurendatakse KLASSI v��rtust.
	}

	public void bark(int barkCount) {
		for (int i = 0; i < barkCount; i++) {
			System.out.println("Barking....");
		}
	}

	public void run() {
		if (age < 5) {
			System.out.println("Run slowly");
		} else {
			System.out.println("Run fast");
		}
	}

	public void printFellowCount() {
		System.out.println("My name is " + this.name + " and I have " + (Dog.count - 1) + " friends");
	}

	public static void printDogCount() {
		System.out.println("You have created " + Dog.count + " dogs so far. Keep on going...");
	}
}
